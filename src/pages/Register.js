import {Fragment, useEffect, useState, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Redirect, useHistory} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register () {
	const {user} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState(0);

	const [isActive, setIsActive] = useState('');
	const history = useHistory();

	useEffect(()=>{
		if ((email!=='' && password1!=='' && password2!=='' && firstName!=='' && lastName!=='' && mobileNo.length>=11)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[email, password1, firstName, lastName, mobileNo])

	let registerUser = e => {
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method:'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res=>res.json())
		.then(data=>{
			if (data) {
				Swal.fire({
					title: `${email} already exist`,
					icon: 'error',
					text: 'Please use a different email.'
				})
				setPassword1('');
				setPassword2('');
			} else {
				fetch('http://localhost:4000/users/register', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						email:email,
						password:password1,
						lastName:lastName,
						firstName:firstName,
						mobileNumber: mobileNo
					})
				})
				.then(res=>res.json())
				.then(data=>{
					setEmail('');
					setPassword1('');
					setPassword2('');
					setFirstName('');
					setLastName('');
					setMobileNo('');

					if (data) {
						Swal.fire({
							title: 'Registration successful',
							icon: 'success',
							text: 'Happy Shopping!'
						})
						history.push('/login')
					} else {
						Swal.fire({
							title: 'Ooops! Something went wrong',
							icon: 'error',
							text: 'Please try again later.'
						})
					}
				})
			}
		})
	}

	return(
		(user.accessToken !== null)?
		<Redirect to='/'/>
		:
		<Fragment>
		<h1>Register</h1>

		<Form onSubmit={(e)=> registerUser(e)}>
			<Form.Group>
				<Form.Label>Email address:</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e=>setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password"
					value={password1}
					onChange={e=>setPassword1(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Verify Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify your password"
					value={password2}
					onChange={e=>setPassword2(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Last Name"
					value={lastName}
					onChange={e=>setLastName(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="First Name"
					value={firstName}
					onChange={e=>setFirstName(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile No.:</Form.Label>
				<Form.Control
				type="text"
					placeholder="11-digit Mobile No."
					value={mobileNo}
					onChange={e=>setMobileNo(e.target.value)}
					required
				/>
			</Form.Group>
			{isActive ?
				<Button type="submit" variant="primary" id="submitBtn">
				Submit
				</Button>
				:
				<Button type="submit" variant="primary" id="submitBtn" disabled>
				Submit
				</Button>
			}
		</Form>
		</Fragment>
	)
}