import {useState, useEffect, Fragment} from 'react';
import ProductCard from './ProductCard';



export default function UserView({productData}){
	const [products, setProducts] = useState([])

	useEffect(()=>{

		const productArray = productData.map(product => {
			if(product.isActive === true){
				return (
					<ProductCard productProp={product} key={product._id}/>
					)
			}else{
				return null;
			}
		})
		setProducts(productArray);

	},[productData])

	return (
		<Fragment>
		{products}
		</Fragment>
	)
}