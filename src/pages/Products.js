import {useState,useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Container} from 'react-bootstrap';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';

export default function Products() {
	const {user} = useContext(UserContext);
	const [allProducts, setAllProducts] = useState([])

	let fetchData = () => {
		fetch('http://localhost:4000/products/getAllProducts')
		.then(res=>res.json())
		.then(data=> {
			setAllProducts(data)
		})
	}

	useEffect(()=> {
		fetchData()
	},[])

	return (
		<Container>
			{
				(user.isAdmin===true) ?
				<AdminView productData={allProducts} fetchData={fetchData}/>
				:
				<header className='text-center mt-3'>
				<h1>All Products</h1>
				<div className='row d-flex flex-wrap justify-content-center align-content-center'>
				<UserView productData={allProducts}/>
				</div>
				</header>
			}
		</Container>
		)
}