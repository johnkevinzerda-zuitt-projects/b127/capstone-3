import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {Card} from 'react-bootstrap';

export default function ProductCard ({productProp}) {
	const {_id, name, description, price} = productProp;

	return(
		<div className='profile-area'>
		<div className='container'>
		<div className='row'>
			<div className='col-4'>
				<Card className='allProducts'>
					<Card.Header className=''></Card.Header>
					<Card.Body className='main-text'>
						<Card.Title className='name'>{name}</Card.Title>
							<div>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							</div>
					</Card.Body>
					<Card.Footer>
					<Link className='btn btn-dark' to={`/products/${_id}`}>More Info</Link>
					</Card.Footer>
				</Card>
			</div>
		</div>
		</div>
		</div>
	)
}

ProductCard.propTypes = {
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}